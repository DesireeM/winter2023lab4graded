import java.util.Scanner;
public class Fridge
{
	Scanner scan = new Scanner (System.in);
	
	private String fruit;
	private String drink;
	private int numEggs;
	
	public String getFruit()
	{
		return fruit;
	}
	
	public void setFruit(String fruit)
	{
		this.fruit = fruit;
	}
	
	
	public String getDrink()
	{
		return drink;
	}
	
	public void setDrink (String drink)
	{
		this.drink = drink;
	}
	
	
	public int getEggs()
	{
		return numEggs;
	}
	
	public void setEggs (int numEggs)
	{
		this.numEggs = numEggs;
	}
	
	
	
	
	
	public int getprintInsideFridge(int numEggs)
	{
		return  numEggs;
	}
	

	
	
	public void takeEggs(int userInput)
	{
		int validValue = validateInput(userInput);
		this.numEggs = this.numEggs - validValue;
	}
	
	public int validateInput(int eggsToTake) //refactor helper method
	{
		if(eggsToTake <= this. numEggs)
		{
			return eggsToTake;
		}
		
		else
		{
			System.out.println("You're taking more eggs than I have. Give a new num: ");
			eggsToTake = scan.nextInt();//what happens if they take too much again?
			validateInput(eggsToTake);
		}
		return eggsToTake;
	}
	
	//Adding a constructor
	
	public Fridge(String fruit, String drink, int numEggs)
	{
		fruit = "strawberry";
		drink = "milk";
		numEggs = 3;
	}
	
}
